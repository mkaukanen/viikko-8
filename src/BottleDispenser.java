
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class BottleDispenser {
    private int bottles;
    private float money;
    private float cost;
    String receipt;
    
private ArrayList<Bottle> bottleTypes = new ArrayList<Bottle>();  

      
    static private BottleDispenser bd = null;
    
    private BottleDispenser() {
        bottles = 5;
        money = 0;
    }
    
    static public BottleDispenser getBD() {
        if (bd == null){
            bd = new BottleDispenser();
        }
        return bd;
    }

    public void initializeInventory(){
        Bottle PepsiMax05 = new Bottle("Pepsi Max", 0.5f, 1.8f);
        Bottle PepsiMax15 = new Bottle("Pepsi Max", 1.5f, 2.2f);
        Bottle CocaColaZero05 = new Bottle("Coca-Cola Zero", 0.5f, 2.0f);
        Bottle CocaColaZero15 = new Bottle("Coca-Cola Zero", 1.5f, 2.5f);
        Bottle FantaZero05 = new Bottle("Fanta Zero", 0.5f, 1.95f);
        bottleTypes.add(PepsiMax05);
        bottleTypes.add(PepsiMax15);
        bottleTypes.add(PepsiMax15);
        bottleTypes.add(CocaColaZero05);
        bottleTypes.add(CocaColaZero15);
        bottleTypes.add(FantaZero05);
    }
    
    public void addMoney(float coin) {
        money += coin;
    }
    public String getMoney(){
        String rounded = String.format ("%.2f", money);
        return rounded;
    }

    public String buyBottle(String type, float size){
        // default bottle code;
        getInventory();
        String returntext ="Väärä valinta!";
        int bottleCode= 0;
        boolean found = false;
        for (int i= 0; i<bottleTypes.size(); i++){
            bottleCode++;
            if(bottleTypes.get(i).name == type && bottleTypes.get(i).size== size){
                found = true;
                break;
            }
        }
        if (found == true){
            try{
            cost = bottleTypes.get(bottleCode-1).price;
            if (money == 0 || money < cost){
                returntext = "Syötä rahaa ensin!";
            }
            else if (bottles == 0){
                returnMoney();
            }

            else{
                bottles -= 1;
                String bottleName= bottleTypes.get(bottleCode-1).name;
                String bottleSize= Float.toString(bottleTypes.get(bottleCode-1).size);
                String bottlePrice= Float.toString(bottleTypes.get(bottleCode-1).price);
                returntext = "KACHUNK! " + bottleName + " tipahti masiinasta!\n";
                DateFormat DF = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                receipt = "*** KUITTI " + DF.format(date) +" ***\r\n\r\n" 
                + bottleName +", "+ bottleSize + "L\t"+ bottlePrice+"€" 
                + "\r\n\r\nKiitos pulloautomaatin käytöstä!";
                removeBottle(bottleCode);
                money = (float) money - cost;
                        }
            }
            finally{
            //shit happens
            }
        }
        else{
        returntext = "Kyseinen tuote on lopussa!";
        }
        return returntext;
    }

    
    public String returnMoney() {
        //format to right output formatting
        String rounded = String.format ("%.2f", money);
        rounded = rounded.replace(".", ",");
        String returntext = "Klink klink. Sinne menivät rahat! Rahaa tuli ulos "+ rounded +"€";
        money = 0;
        return returntext;
    }  
    public String getInventory(){
        int cap;
        cap = bottleTypes.size();
        StringBuilder sb = new StringBuilder();
        for (int index=0; index < cap; index++){
            String type = bottleTypes.get(index).name;
            String size = Float.toString(bottleTypes.get(index).size);
            String price= Float.toString(bottleTypes.get(index).price);
            sb.append(type+" Koko: "+size+" Hinta: "+price+"€\n");
        }
        String inventory = sb.toString();
        return inventory;
    }
        public void removeBottle(int s){
        ListIterator LI = bottleTypes.listIterator();
        for (int i=0; i<s; i++){
            LI.next();
        }
        LI.remove();   
    }
    public ArrayList getbottleTypes(){
        return bottleTypes;
    }
    public String getBottle(int i){
        return bottleTypes.get(i).name;
    }
    public String getSize(int i){
        return Float.toString(bottleTypes.get(i).size);
    }
    
    public String printReceipt() throws IOException{
                String returntext;
        if (receipt == null){
            returntext = "Et ole ostanut vielä mitään!";
        }
        else{
        BufferedWriter out = new BufferedWriter(new FileWriter("Kuitti.txt"));
        out.write(receipt);
        out.close();
        returntext = "Kuitti viimeisimmästä ostoksesta tulostettu!";
        }
        return returntext;

    }
}


