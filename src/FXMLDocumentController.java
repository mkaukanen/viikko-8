/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;


/**
 *
 * @author Miki
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button coinButton;
    @FXML
    private Button buyButton;
    @FXML
    private Button returnButton;
    @FXML
    private Label headerLabel;
    @FXML
    private TextArea bottleViewer;  
    @FXML
    private Label display;
    @FXML
    private Label moneyCount;
    @FXML
    private TextField choiceText;
    @FXML
    private Slider coinAdjust;
    @FXML
    private Label coinID;
      
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            BottleDispenser BD = null;
            BD = BottleDispenser.getBD();
            bottleViewer.setText(BD.getInventory());
            moneyCount.setText("Total: 0€");
            coinID.setText((coinAdjust.getValue())+ "€");
            
    }       
    
    @FXML
    private void coinButtonAction(ActionEvent event) {
        double d = coinAdjust.getValue();
        d = Math.round(d*10) / 10.0;
        String coinValue = Double.toString(d);
        BottleDispenser.getBD().addMoney(Float.parseFloat(coinValue));
        moneyCount.setText("Total: "+ BottleDispenser.getBD().getMoney()+"€");
        display.setText("Klink! Lisää rahaa laitteeseen!");
        coinAdjust.setValue(1);
        coinID.setText((coinAdjust.getValue())+ "€");
 
    }

    @FXML
    private void buyButtonAction(ActionEvent event) {
        try{
        int choice = Integer.parseInt(choiceText.getText());
        String labeltext = BottleDispenser.getBD().buyBottle(choice);
        display.setText(labeltext);
        bottleViewer.setText(BottleDispenser.getBD().getInventory());
        moneyCount.setText("Total: "+ BottleDispenser.getBD().getMoney()+"€");
        }
        catch(RuntimeException e){
             display.setText("Väärä valinta!");
        }       
    }

    @FXML
    private void returnAction(ActionEvent event) {
        String labeltext= BottleDispenser.getBD().returnMoney();
        display.setText(labeltext);
        moneyCount.setText("Total: "+ BottleDispenser.getBD().getMoney()+"€");
    }       

    @FXML
    private void adjustCoinAction(MouseEvent event) {
        double d = coinAdjust.getValue();
        d = Math.round(d*10)/10.0;
        coinID.setText(d+"€");
    }
 
    }
    
