/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;



/**
 *
 * @author Miki
 */
public class FXMLDocumentController_t4 implements Initializable {

    @FXML
    private Button coinButton;
    @FXML
    private Button buyButton;
    @FXML
    private Button returnButton;
    @FXML
    private Label headerLabel;
    @FXML
    private TextArea bottleViewer;  
    @FXML
    private Label display;
    @FXML
    private Label moneyCount;
    private TextField choiceText;
    @FXML
    private Slider coinAdjust;
    @FXML
    private Label coinID;
    @FXML
    private ComboBox<String> bottleChoice;
    @FXML
    private ComboBox<String> sizeChoice;
    @FXML
    private Label notAvailableLabel;
    @FXML
    private Button receiptButton;
      
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            BottleDispenser BD = null;
            BD = BottleDispenser.getBD();
            BottleDispenser.getBD().initializeInventory();
            bottleViewer.setText(BD.getInventory());
            moneyCount.setText("Total: 0€");
            coinID.setText((coinAdjust.getValue())+ "€");
            ArrayList<String> duplicateCheck = new ArrayList<String>();
            for (int i=0; i < BottleDispenser.getBD().getbottleTypes().size(); i++){
                String name = BottleDispenser.getBD().getBottle(i);
                if (!duplicateCheck.contains(name)){
                    bottleChoice.getItems().add(name);
                }
                duplicateCheck.add(name);      
            }   
            for (int i=0; i < BottleDispenser.getBD().getbottleTypes().size(); i++){
                String size= BottleDispenser.getBD().getSize(i);
                if (!duplicateCheck.contains(size)){
                    sizeChoice.getItems().add((size));
                }
                duplicateCheck.add(size);      
            } 
    }       
    
    @FXML
    private void coinButtonAction(ActionEvent event) {
        double d = coinAdjust.getValue();
        d = Math.round(d*10) / 10.0;
        String coinValue = Double.toString(d);
        BottleDispenser.getBD().addMoney(Float.parseFloat(coinValue));
        moneyCount.setText("Total: "+ BottleDispenser.getBD().getMoney()+"€");
        display.setText("Klink! Lisää rahaa laitteeseen!");
        coinAdjust.setValue(1);
        coinID.setText((coinAdjust.getValue())+ "€");
 
 
    }

    @FXML
    private void buyButtonAction(ActionEvent event){
        try{
        String type= bottleChoice.getValue();
        float size = Float.parseFloat(sizeChoice.getValue());
        String labeltext = BottleDispenser.getBD().buyBottle(type, size);
        display.setText(labeltext);
        bottleViewer.setText(BottleDispenser.getBD().getInventory());
        moneyCount.setText("Total: "+ BottleDispenser.getBD().getMoney()+"€");
        bottleChoice.setValue(bottleChoice.getPromptText());
        sizeChoice.setValue(sizeChoice.getPromptText());
        }
        catch(RuntimeException e){
             display.setText("Väärä valinta!");
        }       
    }

    @FXML
    private void returnAction(ActionEvent event) {
        String labeltext= BottleDispenser.getBD().returnMoney();
        display.setText(labeltext);
        moneyCount.setText("Total: "+ BottleDispenser.getBD().getMoney()+"€");
    }       

    @FXML
    private void adjustCoinAction(MouseEvent event) {
        double d = coinAdjust.getValue();
        d = Math.round(d*10)/10.0;
        coinID.setText(d+"€");
    }

    @FXML
    private void receiptAction(ActionEvent event) throws IOException {
        String labeltext = BottleDispenser.getBD().printReceipt();
        display.setText(labeltext);
    }
    

 
    }
    
