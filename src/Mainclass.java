/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Miki
 */
public class Mainclass extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        
        //VKO8 T1-3 launcher
        //Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        //VKO8 T4 launcher
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument_t4.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

